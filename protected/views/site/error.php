    <section id="content">
        <div class="row m-n">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="text-center m-b-lg">
                    <h1 class="h text-white animated bounceInDown"><?php echo $code; ?></h1> </div>
                <div class="list-group m-b-sm bg-white m-b-lg">
                    
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/task/index" class="list-group-item">
                      <i class="fa fa-chevron-right text-muted"></i>
                      <i class="fa fa-fw fa-home text-muted"></i>

                      <?php
                        switch($code) {
                          case 404: {
                            echo "Ничего не найдено";
                          }
                          break;

                          case 403: {
                            echo "Недостаточно прав";
                          }
                          break;
                        }
                      ?>

                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- footer -->
    <footer id="footer">
        <div class="text-center padder clearfix">
            <p> <small><br></small> </p>
        </div>
    </footer>
