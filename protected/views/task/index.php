<div data-modules="common dTable dataTable"></div>
<?php
$this->pageTitle = "";
?>
<br/>
<div class="text-center" id="loader">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/default/712.GIF"/>
</div>
<section class="scrollable wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel content_table"  hidden>
                <header class="panel-heading text-center h3">
                    <div class= "d-table w100per">
                        <div class= "d-table-cel text-right">
                            <?php Render::buttons('task', 'state') ?>
                        </div>
                        <i class="fa fa-info-sign text-muted" data-toggle="popover" data-html="true" data-placement="top" title="" data-trigger="hover" data-original-title="Help"></i> 
                    </div>
                </header>
                <div class="table-responsive anchor-msg">
                    <table id="tasktable" class="table borderless text-left" cellspacing="0" width="100%" >
                        <thead>
                            <tr>
                                <th class=""></th>
                            </tr>
                        </thead>
                        <tbody  id="task-list" class="list-group list-group-sp"></tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
    <?php
    //форма добавления задачи
    $this->renderPartial('_formAddTask', array('task' => $task));
    ?>

</section>
