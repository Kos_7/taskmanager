<section class="panel content_table" hidden>
    <header class="panel-heading font-bold"> Добавить задачу </header>
    <div class="panel-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'formAddTask',
            'enableAjaxValidation' => true,
            'action' => CHtml::normalizeUrl('creatTask'),
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => false,
            ),
            'htmlOptions' => array('class' => 'form-horizontal',
                'onsubmit' => "return false"
            )
        ));
        ?>  
        <div class="form-group">
            <div class="col-sm-7">
                <label class="">Описание задачи<sup class="h6 color-red">*</sup></label>
                <?php
                echo $form->textField($task, 'description', array(
                    "class" => "form-control ",
                    "data-val" => "true",
                    "tabindex" => "1",
                    "autofocus" => true,
                    'placeholder' => 'Опишите задачу',
                ));
                ?>
                <?php echo $form->error($task, 'description', array("class" => "error-label-message", "data-error" => "true")); ?>
            </div>
            <div class="col-sm-3">
                <label class="">Срок выполнения<sup class="h6 color-red">*</sup></label>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $task,
                    'attribute' => 'deadline',
                    'language' => 'ru',
                    'options' => array(
                        'showAnim' => 'slideDown', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'gotoCurrent' => true,
                        'minDate' => 0,
                        'dateFormat' => 'dd-mm-yy',
                        'changeMonth' => true,
                        'changeYear' => false,
                    ),
                    'htmlOptions' => array(
                        'class' => 'form-control',
                        'data-val' => true,
                        'placeholder' => 'дата в формате "день-месяц-год"',
                    ),
                ));
                ?>
                <?php echo $form->error($task, 'deadline', array("class" => "error-label-message", "data-error" => "true")); ?>
            </div>
            <div class="col-sm-2">
                <button id="button_createNewTask" type="button" class="btn btn-success pr-t-r23">Добавить</button> 
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</section>