<div data-modules="common dTable dataTable comment"></div>
<section class="scrollable wrapper">
    <section class="panel anchor-msg">
        <header class="panel-heading font-bold">
            <div class="form-group">
                <div class="pr-t-r10" >
                    <label class="control-label ">
                        <?php
                        echo CHtml::checkBox('status', '', array("class" => 'pr-t2 js-set-status-done', 'disabled' => !$model->status, 'data-id' => $model->id));
                        echo $model->status == 1 ? " Отметить как выполненную" : " Задача выполнена";
                        ?>

                    </label>
                </div>
            </div>
        </header>
        <div class="panel-body">
            <header class="font-bold">Описание задачи:</header>
            <span><?= $model->description ?></span>
            <br>
            <div class="line pull-in"></div>
            <header class="font-bold">Дедлайн:</header>
            <span><?= $model->deadline_format ?></span>
            <div class="line pull-in"></div>
            <header class="font-bold">Комментарии:</header>
                <?php $this->renderPartial("_comment", ['comment' => $comment]); ?>
        </div>
    </section>
</section>
