<div id="comments-body">
    <div class="tab-pane active comment-box hide">
        <article rv-each-comment="comments" class="media m-t-none comment-item">
            <a  class="pull-left thumb-sm m-t-xs ava-receiver" ></a>
            <div class="text-xs block m-t-xs">
                <i class="fa fa-clock-o"></i> { comment.createdAt }
            </div>
            <div class="media-body">
                <a class="pull-left thumb-sm avatar">
                    <img src="<?php echo Yii::app()->params['hrefDefaultAvatar']; ?>" class="img-circle" />
                </a>
                &nbsp;<span rv-unless="comment.canWatchProfile"  class="comment-author-about" style="line-height: 3em">{ comment.author } </span>

                <span class="font-semibold block"> { comment.commentText } </span>
            </div>
            <div class="line line-dashed"></div>
        </article>
    </div>
    <article  class="chat-item field-create-comment">
        <section class="chat-body">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'formAddComment',
                'enableAjaxValidation' => true,
                'action' => CHtml::normalizeUrl('createComment'),
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => false,
                ),
                'htmlOptions' => array('class' => 'form-horizontal',
                    'onsubmit' => "return false"
                )
            ));
            ?>  
            <div class="form-group">
                <div class="col-sm-4">
                    <label class="">Ваше имя<sup class="h6 color-red">*</sup></label>
                    <?php
                    echo $form->textField($comment, 'user_name', array(
                        "class" => "form-control ",
                        "data-val" => "true",
                        "tabindex" => "1",
                        "autofocus" => true,
                        'placeholder' => 'Введите Ваше имя',
                        'rv-on-keypress' => 'sendComment',
                        'rv-value' => 'commentUserName'
                    ));
                    ?>
                    <?php echo $form->error($comment, 'user_name', array("class" => "error-label-message", "data-error" => "true")); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <label class="">Комментарий<sup class="h6 color-red">*</sup></label>
                    <?php
                    echo $form->textArea($comment, 'text', array(
                        "class" => "form-control ",
                        "style" => "resize:none;",
                        "rows" => 4,
                        "data-val" => "true",
                        "tabindex" => "2",
                        "autofocus" => true,
                        'placeholder' => 'Ваш комментарий',
                        'rv-on-keypress' => 'sendComment',
                        'rv-value' => 'commentText'
                    ));
                    ?>
                    <?php echo $form->error($comment, 'text', array("class" => "error-label-message", "data-error" => "true")); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <button rv-on-click="writeComment" class="btn btn-success btn-disbl" type="button">Отправить</button>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </section>

    </article>
    <span class="error-comment" style="display: none; color: red; margin-top: 5px;">{ errorMsg }</span>
</div>
