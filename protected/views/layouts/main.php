<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <title>taskManager</title>
        <meta name="description"
              hbo="app, web app, responsive "/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/_assets/dist/css/app.css" type="text/css"/>
    </head>

    <body>
            <section class="hbox stretch" style=" height: 100%">

                <aside class="bg-success dk aside-sm nav-vertical no-borders" id="nav">
                    <section class="vbox">
                        <header class="dker nav-bar">
                            <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav">
                                <i class="fa fa-bars"></i>
                            </a>

                            <a href="<?php echo $this->createUrl("site/index") ?>" class="nav-brand">TM<sup class="h6">1.0</sup></a>

                        </header>
                        <div style="margin:0 0 100px 0;">
                            <nav class="nav-primary hidden-xs">
                                <ul class="nav">

                                        <li class="dropdown-submenu">
                                            <a href="<?php echo $this->createUrl("task/index") ?>">
                                                <i class="fa fa-tasks"></i> <span>Задачи
                                                    <b class="badge bg-success pull-right j-counter-new-orders"></b>
                                                </span>
                                            </a>
                                        </li>
                                </ul>
                            </nav>
                        </div>
                        <footer class="footer bg-gradient hidden-xs">
                        </footer>
                    </section>
                </aside>
                <?php echo $content; ?>
            </section>

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/_assets/dist/js/vendor.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/_assets/dist/js/init_glue.js"></script>

    </body>

</html>
