<?php
$this->beginContent('//layouts/main');
?>
<section id="content" class="vbox">
    <section class="scrollable">
        <?php echo $content; ?>
    </section>
</section>


<?php $this->endContent(); ?>