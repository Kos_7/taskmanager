create table task(
  id int not null auto_increment, primary key(id),
  description varchar(255),
  status tinyint default 1,
 deadline DATE, 
 created_at DATETIME 
)ENGINE=InnoDB  DEFAULT CHARSET=utf8;

create table comment1(
    id int not null auto_increment, primary key(id),
    id_task int(11) not NULL,
    user_name varchar(50) NOT NULL,
    text varchar(255) NOT NULL,
    created_at DATETIME,
    CONSTRAINT cm_12 FOREIGN KEY(id_task) 
	REFERENCES task(id) 
	ON UPDATE CASCADE ON DELETE CASCADE
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TRIGGER `task_u_d` AFTER DELETE ON `task`
 FOR EACH ROW UPDATE  `updatetable` SET updatetime = NOW( ) WHERE  `table` =  'task';

CREATE TRIGGER `task_u_i` BEFORE INSERT ON `task`
 FOR EACH ROW update `updatetable` set updatetime = now() where `table` = 'task';

CREATE TRIGGER `task_u_u` AFTER UPDATE ON `task`
 FOR EACH ROW UPDATE  `updatetable` SET updatetime = NOW( ) WHERE  `table` =  'task';
