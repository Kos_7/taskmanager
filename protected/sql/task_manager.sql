-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Авг 28 2016 г., 17:52
-- Версия сервера: 5.6.20
-- Версия PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `task_manager`
--
CREATE DATABASE IF NOT EXISTS `task_manager` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `task_manager`;

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
`id` int(11) NOT NULL,
  `id_task` int(11) NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  `user_name` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`id`, `id_task`, `text`, `user_name`, `created_at`) VALUES
(3, 1, 'some text 1', 'Константин', '2016-08-01 11:24:24'),
(4, 2, 'some text 2', 'Константин', '2016-08-02 11:24:24'),
(5, 2, 'some text 3', 'Константин', '2016-08-03 11:24:24'),
(6, 2, 'some text 4', 'Константин', '2016-08-04 11:24:24'),
(7, 3, 'some text 5', 'Константин', '2016-08-05 11:24:24'),
(8, 4, 'some text 6', 'Константин', '2016-08-06 11:24:24'),
(9, 1, 'some text 7', 'Константин', '2016-08-07 11:24:24'),
(10, 2, 'some text 8', 'Константин', '2016-08-08 11:24:24'),
(19, 2, 'some text 9', 'Константин', '2016-08-09 11:24:24'),
(20, 2, 'some text 10', 'Константин', '2016-08-10 11:24:24'),
(21, 3, 'some text 11', 'Игорь', '2016-08-11 11:24:24'),
(22, 4, 'some text 12', 'Константин', '2016-08-12 11:24:24'),
(23, 1, 'some text 13', 'Константин', '2016-08-13 11:24:24'),
(24, 2, 'some text 14', 'Константин', '2016-08-14 11:24:24'),
(25, 4, 'some text 15', 'Константин', '2016-08-19 11:24:24'),
(26, 4, 'some text 16', 'Константин', '2016-08-20 11:24:24'),
(27, 5, 'some text 17', 'Константин', '2016-08-21 11:24:24'),
(28, 5, 'some text 18', 'Константин', '2016-08-22 11:24:24'),
(29, 4, 'some text 19', 'Константин', '2016-08-23 11:24:24'),
(30, 4, 'some text 20', 'Константин', '2016-08-28 11:24:24');

-- --------------------------------------------------------

--
-- Структура таблицы `task`
--

CREATE TABLE IF NOT EXISTS `task` (
`id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `deadline` date NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Дамп данных таблицы `task`
--

INSERT INTO `task` (`id`, `description`, `status`, `deadline`, `created_at`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla id mi non sapien', 0, '2016-08-28', '2016-08-01 00:00:00'),
(2, 'Nunc dui nisl, gravida a porttitor id, semper nec augue. Cras a tellus a tortor tincidunt placerat ', 0, '2016-08-26', '2016-08-03 00:00:00'),
(3, 'In laoreet fringilla eros, vel scelerisque quam scelerisque auctor. Mauris pharetra sapien vitae', 0, '2016-08-30', '2016-08-17 00:00:00'),
(4, 'In auctor consequat nisl, et consequat diam gravida lacinia. Phasellus aliquet dolor at sapien accumsan faucibus.', 0, '2016-08-31', '2016-08-19 00:00:00'),
(5, 'Integer lacinia euismod ipsum, sit amet egestas arcu malesuada consequat. Cras vitae tempus mi. Duis viverra eros sit amet convallis Integer', 0, '2016-08-01', '2016-08-29 00:00:00');

--
-- Триггеры `task`
--
DELIMITER //
CREATE TRIGGER `task_u_d` AFTER DELETE ON `task`
 FOR EACH ROW UPDATE  `updatetable` SET updatetime = NOW( ) WHERE  `table` =  'task'
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `task_u_i` BEFORE INSERT ON `task`
 FOR EACH ROW update `updatetable` set updatetime = now() where `table` = 'task'
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `task_u_u` AFTER UPDATE ON `task`
 FOR EACH ROW UPDATE  `updatetable` SET updatetime = NOW( ) WHERE  `table` =  'task'
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `updatetable`
--

CREATE TABLE IF NOT EXISTS `updatetable` (
  `table` varchar(255) NOT NULL,
  `updatetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `updatetable`
--

INSERT INTO `updatetable` (`table`, `updatetime`) VALUES
('comment', '2016-08-28 18:12:05'),
('task', '2016-08-28 18:48:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
 ADD PRIMARY KEY (`id`), ADD KEY `cm_1` (`id_task`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `updatetable`
--
ALTER TABLE `updatetable`
 ADD PRIMARY KEY (`table`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `comment`
--
ALTER TABLE `comment`
ADD CONSTRAINT `cm_1` FOREIGN KEY (`id_task`) REFERENCES `task` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
