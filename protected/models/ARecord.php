<?php

class ARecord extends CActiveRecord {

    public $deadline_format = '';
    public $day;
    public $month;
    public $year;

    public function behaviors() {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => null
            )
        );
    }

    function trimStr($attribute, $params) {
        if (!function_exists("mb_trim") || !extension_loaded("mbstring")) {
            $res = AGController::mb_trim($this->$attribute);
            $this->$attribute = $res;
            return true;
        } else {
            $res = mb_trim($this->$attribute);
            $this->$attribute = $res;
            return true;
        }
    }

    public function checkDate($attribute, $params) {
        if (empty($this->$attribute)) {
            return true;
        }
        $now = date("d-m-Y");
        $data = str_replace("-", "*", $this->$attribute);
        $data = str_replace("/", "*", $data);
        $data = str_replace("\\", "*", $data);
        $data = str_replace(" ", "*", $data);
        $data = explode('*', $data);

        $this->day = isset($data[0]) ? intval($data[0]) : 0;
        $this->month = isset($data[1]) ? intval($data[1]) : 0;
        $this->year = isset($data[2]) ? intval($data[2]) : 0;

        if (checkdate($this->month, $this->day, $this->year)) {

            $result = (strtotime($this->$attribute) >= strtotime($now) );
            if ($result) {
                return true;
            }
        }
        $error = "Дата некорректна (должна быть больше или равна текущей дате)";
        $this->addError("$attribute", $error);
        return false;
    }

    public function afterFind() {
        if (isset($this->deadline)) {
            $this->deadline_format = Yii::app()->dateFormatter->format('dd-MM-yyyy', $this->deadline);
        }

        parent::afterFind();
    }

}
