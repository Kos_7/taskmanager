<?php

class TaskController extends AGController {

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new Task();
        if (Yii::app()->request->isAjaxRequest) {
            header("Content-Type: application/json; charset=utf-8");
            $typeQuery = Yii::app()->request->getQuery('type');
            if ($typeQuery === 'loadtable') {
                $criteria = $this->createCondition();
                $count = $model->count($criteria);
                $criteria = $this->createLimit($criteria);
                $arrayTasks = $model->findAll($criteria);
                $ajaxArray = $this->formatJson($arrayTasks, $count);

                echo CJSON::encode($ajaxArray);
                Yii::app()->end();
            }
            if ($typeQuery === 'lastupdate') {
                $res = Yii::app()->db->createCommand("select `updatetime` from `updatetable` where `table` = 'task' ")->queryRow();
                if (isset($_POST['lastupdate']) && $_POST['lastupdate'] != $res['updatetime']) {
                    echo 1;
                } else {
                    echo 0;
                }
                Yii::app()->end();
            }
        }
        $this->render('index', ['task' => $model]);
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $model = $this->loadModel($id);
        $comment = new Comment();
        $this->render('view', array(
            'model' => $model,
            'comment' => $comment,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        header("Content-Type: application/json; charset=utf-8");
        $answer = ['status' => 'success'];
        $params = Yii::app()->request->getPost("Task");
        $model = new Task();
        $model->scenario = "createTask";
        $model->attributes = $params;
        $model->status = Yii::app()->params['statusActive'];
        if ($model->validate()) {
            $model->save(false);
        } else {
            $error = CActiveForm::validate($model, null, false);
            if ($error != '[]') {
                $answer = ["status" => "error",
                    'errors' => $error,
                ];
            }
        }
        echo CJSON::encode($answer);
    }

    /**
     * Изменение статуса задачи на "выполнено"
     */
    public function actionSetStatusDoneTask() {
        header("Content-Type: application/json; charset=utf-8");
        $statusActive = Yii::app()->params['statusActive'];
        $statusDone = Yii::app()->params['statusBlocked'];
        $id = intval(Yii::app()->request->getPost("id"));
        $model = Task::model()->find("t.id=:id AND t.status=:statusActive", [":id" => $id, ":statusActive" => $statusActive]);
        if ($model) {
            $model->status = $statusDone;
            if ($model->save(false)) {
                echo CJSON::encode(array('status' => 'success'));
                Yii::app()->end();
            }
        }
    }
    /**
     * получение комментов к задаче
     */
    public function actionGetComments() {
        $taskId = intval(Yii::app()->request->getQuery('taskId'));
        if ($taskId <= 0) {
            AGController::sendResponse(400, ['errors' => 'Неккоректный запрос']);
        }
        $commentHelper = new CommentHelper();
        $flag = "fromTaskId";
        $comments = $commentHelper->getComments($taskId, $flag);
        AGController::sendResponse(200, $comments);
    }

    /**
     * созадние коммента
     */
    public function actionCreateComment() {
        $commentHelper = new CommentHelper();
        $taskId = intval(Yii::app()->request->getPost('taskId'));
        $commentText = Yii::app()->request->getPost('commentText');
        $userName = Yii::app()->request->getPost('commentUserName');
        if ($taskId > 0) {
            $ch = new CommentHelper();
            $comment = $ch->createComment([
                'user_name' => $userName,
                'id_task' => $taskId,
                'text' => $commentText
            ]);

            AGController::sendResponse(200, $comment);
        } else {
            AGController::sendResponse(400, ['errors' => 'Некорректный запрос']);
        }
    }
    
    /**
     * форматирование ответа списка  задач для dt
     */
    protected function formatJson($arrayTasks, $count) {
        $arrayData = array();
        foreach ($arrayTasks as $task) {
            $returnObject = array();
            $returnObject['dataTask']['description'] = $task['description'];
            $returnObject['dataTask']['deadline'] = $task['deadline_format'];
            $returnObject['dataTask']['countComments'] = $task['commentCount'];
            $returnObject['dataTask']['state'] = $task['status'];
            $returnObject['dataTask']['id'] = $task['id'];
            $returnObject['dataTask']['url'] = '/task/view/' . $task['id'];
            $arrayData[] = $returnObject;
        }
        $ajaxArray = array();
        $ajaxArray["recordsTotal"] = $count;
        $ajaxArray["recordsFiltered"] = $count;
        $ajaxArray["data"] = $arrayData;

        $res = Yii::app()->db->createCommand("select `updatetime` from `updatetable` where `table` = 'task' ")->queryRow();
        $ajaxArray["updatetime"] = $res['updatetime'];
        return $ajaxArray;
    }

    protected function createCondition() {
        $condition = array();
        $condition["condition"] = "1";

        $columnSearchValue = null;
        if (isset($_REQUEST['search']['value'])) {
            $columnSearchValue = trim($_REQUEST['search']['value']);
        }

        $limit = 10;
        if (isset($_REQUEST['length'])) {
            $limit = intval($_REQUEST['length']);
        }

        $offset = 0;
        if (isset($_REQUEST['start'])) {
            $offset = intval($_REQUEST['start']);
        }

        $condition["condition"] .= $this->filterOnState();
        if ($columnSearchValue) {
            $condition["condition"] .= " (t.description like :columnSearchValue) ";
            $condition["params"] = array(":columnSearchValue" => '%' . $columnSearchValue . '%');
        }
        return $condition;
    }

    protected function createLimit($condition) {
        $condition['limit'] = isset($_REQUEST['length']) ? $_REQUEST['length'] : 10;
        $condition['offset'] = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
        $condition["order"] = " status asc,  deadline asc ";
        return $condition;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Task the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Task::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'Страница не найдена');
        return $model;
    }

}
