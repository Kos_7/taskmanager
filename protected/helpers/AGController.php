<?php

class AGController extends Controller {

    function __construct($id, $module = null) {
        parent::__construct($id, $module);
        //отключение js скриптов подключаемых yii автоматически (только на тех страницах, которые используют формы yii) 
        $cs = Yii::app()->clientScript;
        $cs->coreScriptPosition = $cs::POS_END;
        $cs->scriptMap = array('jquery.js' => false);
    }

    /**
     * Отправка ответа
     *
     * @param int $codeError - код ответа
     * @param array $response - массив с данными для ответа
     * */
    public static function sendResponse($codeError, $response) {
        header("Content-Type: application/json");
        http_response_code($codeError);
        echo CJSON::encode($response);
        Yii::app()->end();
    }

    /**
     * Фомирование и отправка ошибок валидации
     * @param object $model - модель
     * */
    public static function sendErrorValidateForm($model) {
        $error = CActiveForm::validate($model);
        $error = CJSON::decode($error);
        if ($error != '[]') {
            self::sendResponse(400, [
                "errors" => $error
            ]);
        }
    }

    /**
     * Добавление в запрос условия по стаусу задачи
     * */
    protected function filterOnState() {
        $condition = "";
        $state = Yii::app()->request->getPost('state');
        if ($state == '1') {
            $condition = " AND t.status =" . Yii::app()->params['statusBlocked'];
        } else if ($state == '0') {
            $condition = " AND t.status =" . Yii::app()->params['statusActive'];
        }
        return $condition;
    }

    public static function mb_trim($str) {
        $str = html_entity_decode($str);
        $str = strip_tags($str);
        $str = str_replace("&nbsp;", "", $str);

        return preg_replace("/(^\s+)|(\s+$)/us", "", $str);
    }

}
