<?php

class CommentHelper {
    public function getComments($id, $type) {
        $criteria = new CDbCriteria();
        $criteria->params = [':id' => $id];
        $criteria->order =" created_at asc";
        if ($type == "fromTaskId") {
            $criteria->condition = "id_task=:id";
        } else if ($type == "fromCommentId") {
            $criteria->condition = "id=:id";
        }
        $commentsData = Comment::model()->findAll($criteria);
        return $this->getJsonByData($commentsData);
    }

    public function createComment($attributes) {
        $comment = new Comment();
        $comment->attributes = $attributes;
        if ($comment->validate()) {
            $comment->save(false);
            $type = "fromCommentId";
            return $this->getComments($comment->id, $type);
        } else {
            AGController::sendErrorValidateForm($comment);
        }
    }

    public function getJsonByData($comments) {
        $response['comments'] = [];
        $response['canCommentTask'] = true;

        foreach ($comments as $comment) {
            $data = [
                "commentText" => $comment['text'],
                "createdAt" => $comment['created_at'],
                "author" => $comment['user_name'],
            ];

            array_push($response['comments'], $data);
        }

        return $response;
    }

}
