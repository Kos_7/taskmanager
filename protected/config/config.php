<?php

$host = "localhost";
if (isset($_SERVER) && isset($_SERVER['SERVER_NAME'])) {
    $host = $_SERVER['SERVER_NAME'];
}

define('DB_NAME', 'task_manager');

define('DB_USER', 'root');
define('DB_PASSWORD', '123456');

define('HOST', 'http://' . $host);
define('DEFAULT_AVATAR', '/images/default/default_avatar.jpg');
