<?php
include_once('config.php');

function routeFactory($type, $route, $action) {
    return array($action, 'pattern' => $route, 'verb' => $type);
}
//автоматическое подключение модулей
$dirs = scandir(dirname(__FILE__) . '/../modules');
$modules = array();
foreach ($dirs as $name) {
    if ($name[0] != '.')
        $modules[$name] = array('class' => 'application.modules.' . $name . '.' . ucfirst($name) . 'Module');
}
// строка вида 'news|page|user|...|socials' для подстановки в регулярные выражения общих правил маршрутизации
define('MODULES_MATCHES', implode('|', array_keys($modules)));


return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'taskManager',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.*',
        'application.helpers.*',
    ),
    'modules' => array_replace($modules, array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '123456',
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    )),
    // application components
    'language' => 'ru',
    'defaultController' => 'task',
    'components' => array(
        'user' => array(
            'allowAutoLogin' => true,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                routeFactory('POST', '/api/v1/comments', 'task/createComment'),
                routeFactory('GET', '/api/v1/comments', 'task/getComments'),
                
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>/<id>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
            ),
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=' . DB_NAME,
            'emulatePrepare' => true,
            'username' => DB_USER,
            'password' => DB_PASSWORD,
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
        'errorAction' => 'site/error', //РАСКОММЕНТИРОВАТЬ В РАБОЧЕЙ ВЕРСИИ
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
    'params' => array(
        'statusAll' => -1,   //все задачи
        'statusActive' => 1, //задача ждет выполнения
        'statusBlocked' => 0, //задача выполнена 
        'hrefDefaultAvatar' => HOST . DEFAULT_AVATAR,
    ),
);
