    var App = {
        init: function () {
            if(App.develop_mode){
                console.log('init params');
            }
        },
        
        'statusActive':1,
        'statusBlocked' :0,      
        
        'develop_mode': false, //режим работы сайта - true - разработка ( выводятся сообщения в консоль и отключены автоподгрузка таблиц итд)
        'timeout': 9000, //промежуток между обновлениями таблиц
    };
    module.exports = App;
    
    