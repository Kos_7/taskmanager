var params = require('./params');
var common = require('./common');
var dTable = require('./dTable');
var dataTable = require('./dataTable');
var comment = require('./comment');
//--------------------------------------------------------

var strModules = $('div[data-modules]').data('modules');
if (strModules) {
    var arrModules = strModules.split(" ");

    if (params.develop_mode) {
        console.log('*****start init*****');
        console.log(arrModules);
    }
    //-------------------------------------------------------

    if (arrModules.indexOf("params") != -1) {
        params.init();
    }
    if (arrModules.indexOf("common") != -1) {
        common.init();
    }
    if (arrModules.indexOf("dTable") != -1) {
        dTable.init();
    }
    if (arrModules.indexOf("dataTable") != -1) {
        dataTable.init();
    }
    if (arrModules.indexOf("comment") != -1) {
        comment.init();
    }

} else {
    console.log("Не инициализировны скрипты '/_assets/js/init.js'");
}

window.params = params;
window.common = common;
window.dTable = dTable;
window.dataTable = dataTable;
window.comment = comment;
