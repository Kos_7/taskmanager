var paramsApp = require('./params');
var App = {
    init: function () {

        if (paramsApp.develop_mode) {
            console.log('init common');
        }
    },

    createErrorDiv: function (key, val, selector) {
        var $form = $(selector);
        var errDiv =  $form.find("#" + key + "_em_");
        $form.find("#" + key).css("border-color", "#a94442");
        errDiv.text(val);
        errDiv.css("color", "red");
        errDiv.css("margin-top", "5px");
        errDiv.show();
    },
    clearErrorMessage: function (selector) {
        var $form = $(selector);
        $form.find("[data-val]").css("border", "1px solid #ccc");
        $form.find(" [data-error]").hide();
    },   
    
    showModalTabs: function (parent, modaltype, textMessage, speed) {
        var modal = '<div  id="modalMessage2" class="alert alert-' + modaltype + ' order-tab-mess">' +
                '<button type="button" class="close" data-dismiss="alert">' +
                '<i class="fa fa-times">' +
                '</i>' +
                '</button>' +
                '<i class="fa fa-ban-circle">' +
                '</i>' +
                '<strong>' +
                textMessage +
                '</strong>' +
                '<a href="#" class="alert-link">' +
                '  ' +
                '</a>' +
                ' &nbsp;&nbsp;&nbsp;&nbsp;' +
                '</div>';
        $(parent).prepend(modal);
        speed = speed || 1500;
        var timeOut = setTimeout(function () {
            $("#modalMessage2").fadeOut(2500);
            $("#modalMessage2").remove();
            clearTimeout(timeOut);
        }, 4000);
    },
     sendQuery: function (type, url, dataQuery, cb) {
            $.ajax({
                type: type,
                url: url,
                data: dataQuery,
                dataType: 'json',
                success: function (data, statusText, xhr) {
                    if (cb) {
                        cb(xhr.status, data);
                    }
                },
                error: function (data, statusText, xhr) {
                    if (cb) {
                        var response = JSON.parse(data.responseText);
                        cb(xhr.status, response);
                    }
                }
            });
        },

};
module.exports = App;