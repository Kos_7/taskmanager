var paramsApp = require('./params');
var App = {
    tasktable: null, //таблица задач
    timetable: null, // время обновления таблицы
    init: function () {
        if (paramsApp.develop_mode) {
            console.log('init dTable');
        }

        this._initDtDefaultSettings(); //настройки dt по умолчанию
        this.InitTasktable(); //инициализация и функционал отрисовки таблицы 
        this._initHandler(); //обработчики кнопок и пр
    },
    //настройки DT по умолчанию
    _initDtDefaultSettings: function () {
        $.extend(true, $.fn.dataTable.defaults, {
            "lengthMenu": [10, 25, 50, 75, 100, 200],
            "oLanguage": {
                "sProcessing": false,
                "sLengthMenu": "Показывать  _MENU_  строк",
                "sZeroRecords": "Ничего не найдено",
                "sInfo": "Показано с _START_ по _END_ из _TOTAL_ строк",
                "sInfoEmtpy": "Показано с 0 по 0 из 0 строк",
                "sInfoFiltered": "(выбрано из _MAX_)",
                "sInfoPostFix": "",
                "sSearch": "Поиск: ",
                "sUrl": "",
                "sEmptyTable": "Ничего не найдено",
                "oPaginate": {"sFirst": "First", "sLast": "Last", "sNext": "Следующая", "sPrevious": "Предыдущая"},
                "sInfoEmpty": "Показано с 0 по 0 из 0 строк",
                "sLoadingRecords": "loading..."
            },
            "serverSide": true, /*использовать данные приходящие  с сервера*/
            "bProcessing": true, /*если размер табл большой - отрисовывать по мере прокрутки*/
            "bDeferRender": true, /*если размер табл большой - отрисовывать по мере прокрутки*/
            "bAutoWidth": false,
            "bFilter": true, /**/
            "bInfo": true, /*выводить  под таблицей данные о количестве записей */
            "bSortClasses": true, /*выделять столбец по которому происходит сортировка цветом*/
            "bStateSave": true
        })
    },
    _initHandler: function () {
        //работа с фильтрами стстуса 
        $('body').on('click', '.j-filter', this._setFilterData.bind(this));

    },
    InitTasktable: function () {
        if ($('#tasktable').length != 0) {
            var _this = this;
            _this.tasktable = $('#tasktable').DataTable({
                "ajax": {url: '/task/index/type/loadtable', type: 'POST'},
                "order": [[0, "asc"]],
                "columns": [
                    {data: "dataTask", "orderable": false, render:
                                function (data, type, row)
                                {
                                    var classes = " alert-info  ";
                                    var disabled = "";
                                    var line_through = "";
                                    if (data.state == paramsApp.statusBlocked) {
                                        classes = "alert-done ";
                                        disabled = " disabled ";
                                        line_through = " line-through ";
                                    }
                                    return  '<div class="row alert ' + classes + ' mb0 ml0 mr0">' +
                                            '<div class="col-sm-1 pos-rel-t4 js-set-status-done"><input ' + disabled +
                                            'type="checkbox"  data-state="' + data.state + '" data-id="' + data.id +
                                            '" class="height12 w30"/></div>' +
                                            '<div class="size col-sm-8 ' + line_through + '" > <a href="' + data.url + '">' + data.description +
                                            '</a></div><div class="col-sm-2 pos-rel-t4"><span>' + data.deadline +
                                            '</span></div><div class="col-sm-1 pos-rel-t4 text-center"><span class="badge bg-info">' + data.countComments +
                                            '</span></div>' +
                                            '</div>';
                                }
                    },
                ],
                fnDrawCallback: function (data)
                {
                    _this.timetable = data.json['updatetime'];
                    _this._loader();

                },
                fnServerParams: function (aoData)
                {
                    aoData['state'] = _this.state;
                }
            });
            if (!paramsApp.develop_mode) {
                setInterval(function () {
                    _this._reload('/task/index/type/lastupdate', _this.tasktable);
                }, paramsApp.timeout);
            }
        }
    },
    _loader: function () {
        $('#loader').hide();
        $('.content_table').show();
    },
    _setFilterData: function () {
        var _this = this;
        var elem = event.target;
        var stt = $(elem).data('filter-state');
        if (typeof (stt) !== "undefined") {
            _this.state = stt;
        }
        _this.filter_table = $(elem).data('table-name');
        console.log(_this.filter_table);
        if (_this.filter_table == "task") {
            _this.tasktable.page(0).draw(false);
        }
    },
    _reload: function (url, table)
    {
        var _this = this;
        $.ajax({
            url: url,
            type: "POST",
            data: "lastupdate=" + _this.timetable,
            success: function (msg)
            {
                if (msg == 1)
                {
                    table.draw(false);
                }
            }
        });
    },
};

module.exports.app = App;
module.exports = App;
