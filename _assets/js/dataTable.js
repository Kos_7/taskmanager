var commonApp = require('./common');
var paramsApp = require('./params');
var dTableApp = require('./dTable');

var App = {
    init: function () {
        if (paramsApp.develop_mode) {
            console.log('init dataTable');
        }
        this._initHandler();
    },
    _initHandler: function () {
        //пометить как выполненную
        $('body').on('click', '.js-set-status-done', this._changeStatusDone.bind(this));
        $('#button_createNewTask').on('click', this._saveNewTask.bind(this));
    },
    //создание новой задачи
    _saveNewTask: function () {
        var _this = this;
        $.ajax({
            type: "post",
            data: $('#formAddTask').serialize(),
            url: "/index.php/task/create",
            dataType: "json",
            success: function (data) {
                $("#formAddTask [data-val]").css("border", "1px solid #ccc");
                $("#formAddTask [data-error]").hide();
                if (data.status == "success")
                {
                    if (dTableApp.tasktable) {
                        dTableApp.tasktable.draw(false);
                        _this.cleanForm("#formAddTask");
                    }
                } else if (data.status == "error") {
                    var data1 = JSON.parse(data.errors);
                    $.each(data1, function (key, val)
                    {
                        commonApp.createErrorDiv(key, val, '#formAddTask');
                    });
                }
            }
        });
    },
    cleanForm: function (form) {
        $(form + " [data-val]").val('');
        $(form + "  [data-val]").css("border", "1px solid #ccc");
        $(form + "  [data-error]").hide();
    },
    _changeStatusDone: function (e) {
        var elem = $(e.target);
        var id = elem.data('id');

        if (id) {
            $.ajax({
                type: "post",
                data: "id=" + id,
                url: "/index.php/task/setStatusDoneTask",
                dataType: "json",
                success: function (data) {
                    if (data.status == "success") {
                        if (dTableApp.tasktable){
                            dTableApp.tasktable.draw(false);
                        }else{
                            $('.js-set-status-done').attr('disabled', true);
                        }
                        commonApp.showModalTabs(".anchor-msg", "success", "Задача помечена как выполненная", 1500);


                    } else {
                        commonApp.showModalTabs(".anchor-msg", "danger", "Произошла ошибка", 1500);
                    }
                }
            });
        }
    }
   
};
module.exports = App;

