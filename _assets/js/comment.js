var commonApp = require('./common');
var App = {
    _data: {
        comments: [],
        commentText: '',
        commentUserName: '',
        query: {
            taskId: -1
        },
//        isLoading: false,
//        errorMsg: '',
        sendComment: function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                App.sendComment();
            }
        },
        writeComment: function () {
            App.sendComment();
        }
    },
    init: function () {
        this._initRivets();
        this.getComments();
    },
    sendComment: function () {
        var _this = this;
        //блок кнопки 
        $('.btn-disbl').addClass('disabled');

        _this.showErrors(false, '');
        commonApp.sendQuery('POST', '/api/v1/comments', {
            commentText: _this._data.commentText,
            commentUserName: _this._data.commentUserName,
            taskId: _this._data.query.taskId
        }, function (statusCode, data) {
            if (statusCode === 200) {
                if (data.comments) {
                    _this._data.comments.push(data.comments[0]);
                    commonApp.clearErrorMessage('#formAddComment');
                    _this._data.commentText = '';
                    $('.btn-disbl').removeClass('disabled');
                }
            } else {
                $.each(data.errors, function (key, val)
                {
                    commonApp.createErrorDiv(key, val, '#formAddComment');
                });
                $('.btn-disbl').removeClass('disabled');
            }
        });
    },
    getComments: function () {
        var _this = this;
        _this._data.query.taskId = $('.js-set-status-done').data('id');
        commonApp.sendQuery('GET', '/api/v1/comments', _this._data.query, function (statusCode, data) {
            if (statusCode === 200) {
                _this._data.comments = data.comments;
                _this._data.canCommentTask = data.canCommentTask;
                $('.comment-box').removeClass('hide');

            }
        });
    },
    showErrors: function (hasError, textMsg) {
        $('.error-comment').text(textMsg);

        if (hasError) {
            $('.error-comment').css('display', 'block');
        } else {
            $('.error-comment').css('display', 'none');
        }
    },
    _initRivets: function () {
        rivets.bind($('#comments-body'), this._data);
    }
};
module.exports = App;
