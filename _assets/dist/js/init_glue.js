(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var commonApp = require('./common');
var App = {
    _data: {
        comments: [],
        commentText: '',
        commentUserName: '',
        query: {
            taskId: -1
        },
//        isLoading: false,
//        errorMsg: '',
        sendComment: function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                App.sendComment();
            }
        },
        writeComment: function () {
            App.sendComment();
        }
    },
    init: function () {
        this._initRivets();
        this.getComments();
    },
    sendComment: function () {
        var _this = this;
        //блок кнопки 
        $('.btn-disbl').addClass('disabled');

        _this.showErrors(false, '');
        commonApp.sendQuery('POST', '/api/v1/comments', {
            commentText: _this._data.commentText,
            commentUserName: _this._data.commentUserName,
            taskId: _this._data.query.taskId
        }, function (statusCode, data) {
            if (statusCode === 200) {
                if (data.comments) {
                    _this._data.comments.push(data.comments[0]);
                    commonApp.clearErrorMessage('#formAddComment');
                    _this._data.commentText = '';
                    $('.btn-disbl').removeClass('disabled');
                }
            } else {
                $.each(data.errors, function (key, val)
                {
                    commonApp.createErrorDiv(key, val, '#formAddComment');
                });
                $('.btn-disbl').removeClass('disabled');
            }
        });
    },
    getComments: function () {
        var _this = this;
        _this._data.query.taskId = $('.js-set-status-done').data('id');
        commonApp.sendQuery('GET', '/api/v1/comments', _this._data.query, function (statusCode, data) {
            if (statusCode === 200) {
                _this._data.comments = data.comments;
                _this._data.canCommentTask = data.canCommentTask;
                $('.comment-box').removeClass('hide');

            }
        });
    },
    showErrors: function (hasError, textMsg) {
        $('.error-comment').text(textMsg);

        if (hasError) {
            $('.error-comment').css('display', 'block');
        } else {
            $('.error-comment').css('display', 'none');
        }
    },
    _initRivets: function () {
        rivets.bind($('#comments-body'), this._data);
    }
};
module.exports = App;

},{"./common":2}],2:[function(require,module,exports){
var paramsApp = require('./params');
var App = {
    init: function () {

        if (paramsApp.develop_mode) {
            console.log('init common');
        }
    },

    createErrorDiv: function (key, val, selector) {
        var $form = $(selector);
        var errDiv =  $form.find("#" + key + "_em_");
        $form.find("#" + key).css("border-color", "#a94442");
        errDiv.text(val);
        errDiv.css("color", "red");
        errDiv.css("margin-top", "5px");
        errDiv.show();
    },
    clearErrorMessage: function (selector) {
        var $form = $(selector);
        $form.find("[data-val]").css("border", "1px solid #ccc");
        $form.find(" [data-error]").hide();
    },   
    
    showModalTabs: function (parent, modaltype, textMessage, speed) {
        var modal = '<div  id="modalMessage2" class="alert alert-' + modaltype + ' order-tab-mess">' +
                '<button type="button" class="close" data-dismiss="alert">' +
                '<i class="fa fa-times">' +
                '</i>' +
                '</button>' +
                '<i class="fa fa-ban-circle">' +
                '</i>' +
                '<strong>' +
                textMessage +
                '</strong>' +
                '<a href="#" class="alert-link">' +
                '  ' +
                '</a>' +
                ' &nbsp;&nbsp;&nbsp;&nbsp;' +
                '</div>';
        $(parent).prepend(modal);
        speed = speed || 1500;
        var timeOut = setTimeout(function () {
            $("#modalMessage2").fadeOut(2500);
            $("#modalMessage2").remove();
            clearTimeout(timeOut);
        }, 4000);
    },
     sendQuery: function (type, url, dataQuery, cb) {
            $.ajax({
                type: type,
                url: url,
                data: dataQuery,
                dataType: 'json',
                success: function (data, statusText, xhr) {
                    if (cb) {
                        cb(xhr.status, data);
                    }
                },
                error: function (data, statusText, xhr) {
                    if (cb) {
                        var response = JSON.parse(data.responseText);
                        cb(xhr.status, response);
                    }
                }
            });
        },

};
module.exports = App;
},{"./params":6}],3:[function(require,module,exports){
var paramsApp = require('./params');
var App = {
    tasktable: null, //таблица задач
    timetable: null, // время обновления таблицы
    init: function () {
        if (paramsApp.develop_mode) {
            console.log('init dTable');
        }

        this._initDtDefaultSettings(); //настройки dt по умолчанию
        this.InitTasktable(); //инициализация и функционал отрисовки таблицы 
        this._initHandler(); //обработчики кнопок и пр
    },
    //настройки DT по умолчанию
    _initDtDefaultSettings: function () {
        $.extend(true, $.fn.dataTable.defaults, {
            "lengthMenu": [10, 25, 50, 75, 100, 200],
            "oLanguage": {
                "sProcessing": false,
                "sLengthMenu": "Показывать  _MENU_  строк",
                "sZeroRecords": "Ничего не найдено",
                "sInfo": "Показано с _START_ по _END_ из _TOTAL_ строк",
                "sInfoEmtpy": "Показано с 0 по 0 из 0 строк",
                "sInfoFiltered": "(выбрано из _MAX_)",
                "sInfoPostFix": "",
                "sSearch": "Поиск: ",
                "sUrl": "",
                "sEmptyTable": "Ничего не найдено",
                "oPaginate": {"sFirst": "First", "sLast": "Last", "sNext": "Следующая", "sPrevious": "Предыдущая"},
                "sInfoEmpty": "Показано с 0 по 0 из 0 строк",
                "sLoadingRecords": "loading..."
            },
            "serverSide": true, /*использовать данные приходящие  с сервера*/
            "bProcessing": true, /*если размер табл большой - отрисовывать по мере прокрутки*/
            "bDeferRender": true, /*если размер табл большой - отрисовывать по мере прокрутки*/
            "bAutoWidth": false,
            "bFilter": true, /**/
            "bInfo": true, /*выводить  под таблицей данные о количестве записей */
            "bSortClasses": true, /*выделять столбец по которому происходит сортировка цветом*/
            "bStateSave": true
        })
    },
    _initHandler: function () {
        //работа с фильтрами стстуса 
        $('body').on('click', '.j-filter', this._setFilterData.bind(this));

    },
    InitTasktable: function () {
        if ($('#tasktable').length != 0) {
            var _this = this;
            _this.tasktable = $('#tasktable').DataTable({
                "ajax": {url: '/task/index/type/loadtable', type: 'POST'},
                "order": [[0, "asc"]],
                "columns": [
                    {data: "dataTask", "orderable": false, render:
                                function (data, type, row)
                                {
                                    var classes = " alert-info  ";
                                    var disabled = "";
                                    var line_through = "";
                                    if (data.state == paramsApp.statusBlocked) {
                                        classes = "alert-done ";
                                        disabled = " disabled ";
                                        line_through = " line-through ";
                                    }
                                    return  '<div class="row alert ' + classes + ' mb0 ml0 mr0">' +
                                            '<div class="col-sm-1 pos-rel-t4 js-set-status-done"><input ' + disabled +
                                            'type="checkbox"  data-state="' + data.state + '" data-id="' + data.id +
                                            '" class="height12 w30"/></div>' +
                                            '<div class="size col-sm-8 ' + line_through + '" > <a href="' + data.url + '">' + data.description +
                                            '</a></div><div class="col-sm-2 pos-rel-t4"><span>' + data.deadline +
                                            '</span></div><div class="col-sm-1 pos-rel-t4 text-center"><span class="badge bg-info">' + data.countComments +
                                            '</span></div>' +
                                            '</div>';
                                }
                    },
                ],
                fnDrawCallback: function (data)
                {
                    _this.timetable = data.json['updatetime'];
                    _this._loader();

                },
                fnServerParams: function (aoData)
                {
                    aoData['state'] = _this.state;
                }
            });
            if (!paramsApp.develop_mode) {
                setInterval(function () {
                    _this._reload('/task/index/type/lastupdate', _this.tasktable);
                }, paramsApp.timeout);
            }
        }
    },
    _loader: function () {
        $('#loader').hide();
        $('.content_table').show();
    },
    _setFilterData: function () {
        var _this = this;
        var elem = event.target;
        var stt = $(elem).data('filter-state');
        if (typeof (stt) !== "undefined") {
            _this.state = stt;
        }
        _this.filter_table = $(elem).data('table-name');
        console.log(_this.filter_table);
        if (_this.filter_table == "task") {
            _this.tasktable.page(0).draw(false);
        }
    },
    _reload: function (url, table)
    {
        var _this = this;
        $.ajax({
            url: url,
            type: "POST",
            data: "lastupdate=" + _this.timetable,
            success: function (msg)
            {
                if (msg == 1)
                {
                    table.draw(false);
                }
            }
        });
    },
};

module.exports.app = App;
module.exports = App;

},{"./params":6}],4:[function(require,module,exports){
var commonApp = require('./common');
var paramsApp = require('./params');
var dTableApp = require('./dTable');

var App = {
    init: function () {
        if (paramsApp.develop_mode) {
            console.log('init dataTable');
        }
        this._initHandler();
    },
    _initHandler: function () {
        //пометить как выполненную
        $('body').on('click', '.js-set-status-done', this._changeStatusDone.bind(this));
        $('#button_createNewTask').on('click', this._saveNewTask.bind(this));
    },
    //создание новой задачи
    _saveNewTask: function () {
        var _this = this;
        $.ajax({
            type: "post",
            data: $('#formAddTask').serialize(),
            url: "/index.php/task/create",
            dataType: "json",
            success: function (data) {
                $("#formAddTask [data-val]").css("border", "1px solid #ccc");
                $("#formAddTask [data-error]").hide();
                if (data.status == "success")
                {
                    if (dTableApp.tasktable) {
                        dTableApp.tasktable.draw(false);
                        _this.cleanForm("#formAddTask");
                    }
                } else if (data.status == "error") {
                    var data1 = JSON.parse(data.errors);
                    $.each(data1, function (key, val)
                    {
                        commonApp.createErrorDiv(key, val, '#formAddTask');
                    });
                }
            }
        });
    },
    cleanForm: function (form) {
        $(form + " [data-val]").val('');
        $(form + "  [data-val]").css("border", "1px solid #ccc");
        $(form + "  [data-error]").hide();
    },
    _changeStatusDone: function (e) {
        var elem = $(e.target);
        var id = elem.data('id');

        if (id) {
            $.ajax({
                type: "post",
                data: "id=" + id,
                url: "/index.php/task/setStatusDoneTask",
                dataType: "json",
                success: function (data) {
                    if (data.status == "success") {
                        if (dTableApp.tasktable){
                            dTableApp.tasktable.draw(false);
                        }else{
                            $('.js-set-status-done').attr('disabled', true);
                        }
                        commonApp.showModalTabs(".anchor-msg", "success", "Задача помечена как выполненная", 1500);


                    } else {
                        commonApp.showModalTabs(".anchor-msg", "danger", "Произошла ошибка", 1500);
                    }
                }
            });
        }
    }
   
};
module.exports = App;


},{"./common":2,"./dTable":3,"./params":6}],5:[function(require,module,exports){
var params = require('./params');
var common = require('./common');
var dTable = require('./dTable');
var dataTable = require('./dataTable');
var comment = require('./comment');
//--------------------------------------------------------

var strModules = $('div[data-modules]').data('modules');
if (strModules) {
    var arrModules = strModules.split(" ");

    if (params.develop_mode) {
        console.log('*****start init*****');
        console.log(arrModules);
    }
    //-------------------------------------------------------

    if (arrModules.indexOf("params") != -1) {
        params.init();
    }
    if (arrModules.indexOf("common") != -1) {
        common.init();
    }
    if (arrModules.indexOf("dTable") != -1) {
        dTable.init();
    }
    if (arrModules.indexOf("dataTable") != -1) {
        dataTable.init();
    }
    if (arrModules.indexOf("comment") != -1) {
        comment.init();
    }

} else {
    console.log("Не инициализировны скрипты '/_assets/js/init.js'");
}

window.params = params;
window.common = common;
window.dTable = dTable;
window.dataTable = dataTable;
window.comment = comment;

},{"./comment":1,"./common":2,"./dTable":3,"./dataTable":4,"./params":6}],6:[function(require,module,exports){
    var App = {
        init: function () {
            if(App.develop_mode){
                console.log('init params');
            }
        },
        
        'statusActive':1,
        'statusBlocked' :0,      
        
        'develop_mode': false, //режим работы сайта - true - разработка ( выводятся сообщения в консоль и отключены автоподгрузка таблиц итд)
        'timeout': 9000, //промежуток между обновлениями таблиц
    };
    module.exports = App;
    
    
},{}]},{},[5])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJqcy9jb21tZW50LmpzIiwianMvY29tbW9uLmpzIiwianMvZFRhYmxlLmpzIiwianMvZGF0YVRhYmxlLmpzIiwianMvaW5pdC5qcyIsImpzL3BhcmFtcy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNyRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcElBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDL0VBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJ2YXIgY29tbW9uQXBwID0gcmVxdWlyZSgnLi9jb21tb24nKTtcbnZhciBBcHAgPSB7XG4gICAgX2RhdGE6IHtcbiAgICAgICAgY29tbWVudHM6IFtdLFxuICAgICAgICBjb21tZW50VGV4dDogJycsXG4gICAgICAgIGNvbW1lbnRVc2VyTmFtZTogJycsXG4gICAgICAgIHF1ZXJ5OiB7XG4gICAgICAgICAgICB0YXNrSWQ6IC0xXG4gICAgICAgIH0sXG4vLyAgICAgICAgaXNMb2FkaW5nOiBmYWxzZSxcbi8vICAgICAgICBlcnJvck1zZzogJycsXG4gICAgICAgIHNlbmRDb21tZW50OiBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgaWYgKGUua2V5Q29kZSA9PT0gMTMpIHtcbiAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgQXBwLnNlbmRDb21tZW50KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHdyaXRlQ29tbWVudDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgQXBwLnNlbmRDb21tZW50KCk7XG4gICAgICAgIH1cbiAgICB9LFxuICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5faW5pdFJpdmV0cygpO1xuICAgICAgICB0aGlzLmdldENvbW1lbnRzKCk7XG4gICAgfSxcbiAgICBzZW5kQ29tbWVudDogZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAvL9Cx0LvQvtC6INC60L3QvtC/0LrQuCBcbiAgICAgICAgJCgnLmJ0bi1kaXNibCcpLmFkZENsYXNzKCdkaXNhYmxlZCcpO1xuXG4gICAgICAgIF90aGlzLnNob3dFcnJvcnMoZmFsc2UsICcnKTtcbiAgICAgICAgY29tbW9uQXBwLnNlbmRRdWVyeSgnUE9TVCcsICcvYXBpL3YxL2NvbW1lbnRzJywge1xuICAgICAgICAgICAgY29tbWVudFRleHQ6IF90aGlzLl9kYXRhLmNvbW1lbnRUZXh0LFxuICAgICAgICAgICAgY29tbWVudFVzZXJOYW1lOiBfdGhpcy5fZGF0YS5jb21tZW50VXNlck5hbWUsXG4gICAgICAgICAgICB0YXNrSWQ6IF90aGlzLl9kYXRhLnF1ZXJ5LnRhc2tJZFxuICAgICAgICB9LCBmdW5jdGlvbiAoc3RhdHVzQ29kZSwgZGF0YSkge1xuICAgICAgICAgICAgaWYgKHN0YXR1c0NvZGUgPT09IDIwMCkge1xuICAgICAgICAgICAgICAgIGlmIChkYXRhLmNvbW1lbnRzKSB7XG4gICAgICAgICAgICAgICAgICAgIF90aGlzLl9kYXRhLmNvbW1lbnRzLnB1c2goZGF0YS5jb21tZW50c1swXSk7XG4gICAgICAgICAgICAgICAgICAgIGNvbW1vbkFwcC5jbGVhckVycm9yTWVzc2FnZSgnI2Zvcm1BZGRDb21tZW50Jyk7XG4gICAgICAgICAgICAgICAgICAgIF90aGlzLl9kYXRhLmNvbW1lbnRUZXh0ID0gJyc7XG4gICAgICAgICAgICAgICAgICAgICQoJy5idG4tZGlzYmwnKS5yZW1vdmVDbGFzcygnZGlzYWJsZWQnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICQuZWFjaChkYXRhLmVycm9ycywgZnVuY3Rpb24gKGtleSwgdmFsKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgY29tbW9uQXBwLmNyZWF0ZUVycm9yRGl2KGtleSwgdmFsLCAnI2Zvcm1BZGRDb21tZW50Jyk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgJCgnLmJ0bi1kaXNibCcpLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9LFxuICAgIGdldENvbW1lbnRzOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgIF90aGlzLl9kYXRhLnF1ZXJ5LnRhc2tJZCA9ICQoJy5qcy1zZXQtc3RhdHVzLWRvbmUnKS5kYXRhKCdpZCcpO1xuICAgICAgICBjb21tb25BcHAuc2VuZFF1ZXJ5KCdHRVQnLCAnL2FwaS92MS9jb21tZW50cycsIF90aGlzLl9kYXRhLnF1ZXJ5LCBmdW5jdGlvbiAoc3RhdHVzQ29kZSwgZGF0YSkge1xuICAgICAgICAgICAgaWYgKHN0YXR1c0NvZGUgPT09IDIwMCkge1xuICAgICAgICAgICAgICAgIF90aGlzLl9kYXRhLmNvbW1lbnRzID0gZGF0YS5jb21tZW50cztcbiAgICAgICAgICAgICAgICBfdGhpcy5fZGF0YS5jYW5Db21tZW50VGFzayA9IGRhdGEuY2FuQ29tbWVudFRhc2s7XG4gICAgICAgICAgICAgICAgJCgnLmNvbW1lbnQtYm94JykucmVtb3ZlQ2xhc3MoJ2hpZGUnKTtcblxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9LFxuICAgIHNob3dFcnJvcnM6IGZ1bmN0aW9uIChoYXNFcnJvciwgdGV4dE1zZykge1xuICAgICAgICAkKCcuZXJyb3ItY29tbWVudCcpLnRleHQodGV4dE1zZyk7XG5cbiAgICAgICAgaWYgKGhhc0Vycm9yKSB7XG4gICAgICAgICAgICAkKCcuZXJyb3ItY29tbWVudCcpLmNzcygnZGlzcGxheScsICdibG9jaycpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJCgnLmVycm9yLWNvbW1lbnQnKS5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgICAgICB9XG4gICAgfSxcbiAgICBfaW5pdFJpdmV0czogZnVuY3Rpb24gKCkge1xuICAgICAgICByaXZldHMuYmluZCgkKCcjY29tbWVudHMtYm9keScpLCB0aGlzLl9kYXRhKTtcbiAgICB9XG59O1xubW9kdWxlLmV4cG9ydHMgPSBBcHA7XG4iLCJ2YXIgcGFyYW1zQXBwID0gcmVxdWlyZSgnLi9wYXJhbXMnKTtcbnZhciBBcHAgPSB7XG4gICAgaW5pdDogZnVuY3Rpb24gKCkge1xuXG4gICAgICAgIGlmIChwYXJhbXNBcHAuZGV2ZWxvcF9tb2RlKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnaW5pdCBjb21tb24nKTtcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBjcmVhdGVFcnJvckRpdjogZnVuY3Rpb24gKGtleSwgdmFsLCBzZWxlY3Rvcikge1xuICAgICAgICB2YXIgJGZvcm0gPSAkKHNlbGVjdG9yKTtcbiAgICAgICAgdmFyIGVyckRpdiA9ICAkZm9ybS5maW5kKFwiI1wiICsga2V5ICsgXCJfZW1fXCIpO1xuICAgICAgICAkZm9ybS5maW5kKFwiI1wiICsga2V5KS5jc3MoXCJib3JkZXItY29sb3JcIiwgXCIjYTk0NDQyXCIpO1xuICAgICAgICBlcnJEaXYudGV4dCh2YWwpO1xuICAgICAgICBlcnJEaXYuY3NzKFwiY29sb3JcIiwgXCJyZWRcIik7XG4gICAgICAgIGVyckRpdi5jc3MoXCJtYXJnaW4tdG9wXCIsIFwiNXB4XCIpO1xuICAgICAgICBlcnJEaXYuc2hvdygpO1xuICAgIH0sXG4gICAgY2xlYXJFcnJvck1lc3NhZ2U6IGZ1bmN0aW9uIChzZWxlY3Rvcikge1xuICAgICAgICB2YXIgJGZvcm0gPSAkKHNlbGVjdG9yKTtcbiAgICAgICAgJGZvcm0uZmluZChcIltkYXRhLXZhbF1cIikuY3NzKFwiYm9yZGVyXCIsIFwiMXB4IHNvbGlkICNjY2NcIik7XG4gICAgICAgICRmb3JtLmZpbmQoXCIgW2RhdGEtZXJyb3JdXCIpLmhpZGUoKTtcbiAgICB9LCAgIFxuICAgIFxuICAgIHNob3dNb2RhbFRhYnM6IGZ1bmN0aW9uIChwYXJlbnQsIG1vZGFsdHlwZSwgdGV4dE1lc3NhZ2UsIHNwZWVkKSB7XG4gICAgICAgIHZhciBtb2RhbCA9ICc8ZGl2ICBpZD1cIm1vZGFsTWVzc2FnZTJcIiBjbGFzcz1cImFsZXJ0IGFsZXJ0LScgKyBtb2RhbHR5cGUgKyAnIG9yZGVyLXRhYi1tZXNzXCI+JyArXG4gICAgICAgICAgICAgICAgJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiY2xvc2VcIiBkYXRhLWRpc21pc3M9XCJhbGVydFwiPicgK1xuICAgICAgICAgICAgICAgICc8aSBjbGFzcz1cImZhIGZhLXRpbWVzXCI+JyArXG4gICAgICAgICAgICAgICAgJzwvaT4nICtcbiAgICAgICAgICAgICAgICAnPC9idXR0b24+JyArXG4gICAgICAgICAgICAgICAgJzxpIGNsYXNzPVwiZmEgZmEtYmFuLWNpcmNsZVwiPicgK1xuICAgICAgICAgICAgICAgICc8L2k+JyArXG4gICAgICAgICAgICAgICAgJzxzdHJvbmc+JyArXG4gICAgICAgICAgICAgICAgdGV4dE1lc3NhZ2UgK1xuICAgICAgICAgICAgICAgICc8L3N0cm9uZz4nICtcbiAgICAgICAgICAgICAgICAnPGEgaHJlZj1cIiNcIiBjbGFzcz1cImFsZXJ0LWxpbmtcIj4nICtcbiAgICAgICAgICAgICAgICAnICAnICtcbiAgICAgICAgICAgICAgICAnPC9hPicgK1xuICAgICAgICAgICAgICAgICcgJm5ic3A7Jm5ic3A7Jm5ic3A7Jm5ic3A7JyArXG4gICAgICAgICAgICAgICAgJzwvZGl2Pic7XG4gICAgICAgICQocGFyZW50KS5wcmVwZW5kKG1vZGFsKTtcbiAgICAgICAgc3BlZWQgPSBzcGVlZCB8fCAxNTAwO1xuICAgICAgICB2YXIgdGltZU91dCA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJChcIiNtb2RhbE1lc3NhZ2UyXCIpLmZhZGVPdXQoMjUwMCk7XG4gICAgICAgICAgICAkKFwiI21vZGFsTWVzc2FnZTJcIikucmVtb3ZlKCk7XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQodGltZU91dCk7XG4gICAgICAgIH0sIDQwMDApO1xuICAgIH0sXG4gICAgIHNlbmRRdWVyeTogZnVuY3Rpb24gKHR5cGUsIHVybCwgZGF0YVF1ZXJ5LCBjYikge1xuICAgICAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgICAgICB0eXBlOiB0eXBlLFxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIGRhdGE6IGRhdGFRdWVyeSxcbiAgICAgICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLFxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChkYXRhLCBzdGF0dXNUZXh0LCB4aHIpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGNiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjYih4aHIuc3RhdHVzLCBkYXRhKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uIChkYXRhLCBzdGF0dXNUZXh0LCB4aHIpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGNiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgcmVzcG9uc2UgPSBKU09OLnBhcnNlKGRhdGEucmVzcG9uc2VUZXh0KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNiKHhoci5zdGF0dXMsIHJlc3BvbnNlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuXG59O1xubW9kdWxlLmV4cG9ydHMgPSBBcHA7IiwidmFyIHBhcmFtc0FwcCA9IHJlcXVpcmUoJy4vcGFyYW1zJyk7XG52YXIgQXBwID0ge1xuICAgIHRhc2t0YWJsZTogbnVsbCwgLy/RgtCw0LHQu9C40YbQsCDQt9Cw0LTQsNGHXG4gICAgdGltZXRhYmxlOiBudWxsLCAvLyDQstGA0LXQvNGPINC+0LHQvdC+0LLQu9C10L3QuNGPINGC0LDQsdC70LjRhtGLXG4gICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAocGFyYW1zQXBwLmRldmVsb3BfbW9kZSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ2luaXQgZFRhYmxlJyk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLl9pbml0RHREZWZhdWx0U2V0dGluZ3MoKTsgLy/QvdCw0YHRgtGA0L7QudC60LggZHQg0L/QviDRg9C80L7Qu9GH0LDQvdC40Y5cbiAgICAgICAgdGhpcy5Jbml0VGFza3RhYmxlKCk7IC8v0LjQvdC40YbQuNCw0LvQuNC30LDRhtC40Y8g0Lgg0YTRg9C90LrRhtC40L7QvdCw0Lsg0L7RgtGA0LjRgdC+0LLQutC4INGC0LDQsdC70LjRhtGLIFxuICAgICAgICB0aGlzLl9pbml0SGFuZGxlcigpOyAvL9C+0LHRgNCw0LHQvtGC0YfQuNC60Lgg0LrQvdC+0L/QvtC6INC4INC/0YBcbiAgICB9LFxuICAgIC8v0L3QsNGB0YLRgNC+0LnQutC4IERUINC/0L4g0YPQvNC+0LvRh9Cw0L3QuNGOXG4gICAgX2luaXREdERlZmF1bHRTZXR0aW5nczogZnVuY3Rpb24gKCkge1xuICAgICAgICAkLmV4dGVuZCh0cnVlLCAkLmZuLmRhdGFUYWJsZS5kZWZhdWx0cywge1xuICAgICAgICAgICAgXCJsZW5ndGhNZW51XCI6IFsxMCwgMjUsIDUwLCA3NSwgMTAwLCAyMDBdLFxuICAgICAgICAgICAgXCJvTGFuZ3VhZ2VcIjoge1xuICAgICAgICAgICAgICAgIFwic1Byb2Nlc3NpbmdcIjogZmFsc2UsXG4gICAgICAgICAgICAgICAgXCJzTGVuZ3RoTWVudVwiOiBcItCf0L7QutCw0LfRi9Cy0LDRgtGMICBfTUVOVV8gINGB0YLRgNC+0LpcIixcbiAgICAgICAgICAgICAgICBcInNaZXJvUmVjb3Jkc1wiOiBcItCd0LjRh9C10LPQviDQvdC1INC90LDQudC00LXQvdC+XCIsXG4gICAgICAgICAgICAgICAgXCJzSW5mb1wiOiBcItCf0L7QutCw0LfQsNC90L4g0YEgX1NUQVJUXyDQv9C+IF9FTkRfINC40LcgX1RPVEFMXyDRgdGC0YDQvtC6XCIsXG4gICAgICAgICAgICAgICAgXCJzSW5mb0VtdHB5XCI6IFwi0J/QvtC60LDQt9Cw0L3QviDRgSAwINC/0L4gMCDQuNC3IDAg0YHRgtGA0L7QulwiLFxuICAgICAgICAgICAgICAgIFwic0luZm9GaWx0ZXJlZFwiOiBcIijQstGL0LHRgNCw0L3QviDQuNC3IF9NQVhfKVwiLFxuICAgICAgICAgICAgICAgIFwic0luZm9Qb3N0Rml4XCI6IFwiXCIsXG4gICAgICAgICAgICAgICAgXCJzU2VhcmNoXCI6IFwi0J/QvtC40YHQujogXCIsXG4gICAgICAgICAgICAgICAgXCJzVXJsXCI6IFwiXCIsXG4gICAgICAgICAgICAgICAgXCJzRW1wdHlUYWJsZVwiOiBcItCd0LjRh9C10LPQviDQvdC1INC90LDQudC00LXQvdC+XCIsXG4gICAgICAgICAgICAgICAgXCJvUGFnaW5hdGVcIjoge1wic0ZpcnN0XCI6IFwiRmlyc3RcIiwgXCJzTGFzdFwiOiBcIkxhc3RcIiwgXCJzTmV4dFwiOiBcItCh0LvQtdC00YPRjtGJ0LDRj1wiLCBcInNQcmV2aW91c1wiOiBcItCf0YDQtdC00YvQtNGD0YnQsNGPXCJ9LFxuICAgICAgICAgICAgICAgIFwic0luZm9FbXB0eVwiOiBcItCf0L7QutCw0LfQsNC90L4g0YEgMCDQv9C+IDAg0LjQtyAwINGB0YLRgNC+0LpcIixcbiAgICAgICAgICAgICAgICBcInNMb2FkaW5nUmVjb3Jkc1wiOiBcImxvYWRpbmcuLi5cIlxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFwic2VydmVyU2lkZVwiOiB0cnVlLCAvKtC40YHQv9C+0LvRjNC30L7QstCw0YLRjCDQtNCw0L3QvdGL0LUg0L/RgNC40YXQvtC00Y/RidC40LUgINGBINGB0LXRgNCy0LXRgNCwKi9cbiAgICAgICAgICAgIFwiYlByb2Nlc3NpbmdcIjogdHJ1ZSwgLyrQtdGB0LvQuCDRgNCw0LfQvNC10YAg0YLQsNCx0Lsg0LHQvtC70YzRiNC+0LkgLSDQvtGC0YDQuNGB0L7QstGL0LLQsNGC0Ywg0L/QviDQvNC10YDQtSDQv9GA0L7QutGA0YPRgtC60LgqL1xuICAgICAgICAgICAgXCJiRGVmZXJSZW5kZXJcIjogdHJ1ZSwgLyrQtdGB0LvQuCDRgNCw0LfQvNC10YAg0YLQsNCx0Lsg0LHQvtC70YzRiNC+0LkgLSDQvtGC0YDQuNGB0L7QstGL0LLQsNGC0Ywg0L/QviDQvNC10YDQtSDQv9GA0L7QutGA0YPRgtC60LgqL1xuICAgICAgICAgICAgXCJiQXV0b1dpZHRoXCI6IGZhbHNlLFxuICAgICAgICAgICAgXCJiRmlsdGVyXCI6IHRydWUsIC8qKi9cbiAgICAgICAgICAgIFwiYkluZm9cIjogdHJ1ZSwgLyrQstGL0LLQvtC00LjRgtGMICDQv9C+0LQg0YLQsNCx0LvQuNGG0LXQuSDQtNCw0L3QvdGL0LUg0L4g0LrQvtC70LjRh9C10YHRgtCy0LUg0LfQsNC/0LjRgdC10LkgKi9cbiAgICAgICAgICAgIFwiYlNvcnRDbGFzc2VzXCI6IHRydWUsIC8q0LLRi9C00LXQu9GP0YLRjCDRgdGC0L7Qu9Cx0LXRhiDQv9C+INC60L7RgtC+0YDQvtC80YMg0L/RgNC+0LjRgdGF0L7QtNC40YIg0YHQvtGA0YLQuNGA0L7QstC60LAg0YbQstC10YLQvtC8Ki9cbiAgICAgICAgICAgIFwiYlN0YXRlU2F2ZVwiOiB0cnVlXG4gICAgICAgIH0pXG4gICAgfSxcbiAgICBfaW5pdEhhbmRsZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgLy/RgNCw0LHQvtGC0LAg0YEg0YTQuNC70YzRgtGA0LDQvNC4INGB0YLRgdGC0YPRgdCwIFxuICAgICAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgJy5qLWZpbHRlcicsIHRoaXMuX3NldEZpbHRlckRhdGEuYmluZCh0aGlzKSk7XG5cbiAgICB9LFxuICAgIEluaXRUYXNrdGFibGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKCQoJyN0YXNrdGFibGUnKS5sZW5ndGggIT0gMCkge1xuICAgICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICAgIF90aGlzLnRhc2t0YWJsZSA9ICQoJyN0YXNrdGFibGUnKS5EYXRhVGFibGUoe1xuICAgICAgICAgICAgICAgIFwiYWpheFwiOiB7dXJsOiAnL3Rhc2svaW5kZXgvdHlwZS9sb2FkdGFibGUnLCB0eXBlOiAnUE9TVCd9LFxuICAgICAgICAgICAgICAgIFwib3JkZXJcIjogW1swLCBcImFzY1wiXV0sXG4gICAgICAgICAgICAgICAgXCJjb2x1bW5zXCI6IFtcbiAgICAgICAgICAgICAgICAgICAge2RhdGE6IFwiZGF0YVRhc2tcIiwgXCJvcmRlcmFibGVcIjogZmFsc2UsIHJlbmRlcjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gKGRhdGEsIHR5cGUsIHJvdylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGNsYXNzZXMgPSBcIiBhbGVydC1pbmZvICBcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBkaXNhYmxlZCA9IFwiXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgbGluZV90aHJvdWdoID0gXCJcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLnN0YXRlID09IHBhcmFtc0FwcC5zdGF0dXNCbG9ja2VkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NlcyA9IFwiYWxlcnQtZG9uZSBcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZCA9IFwiIGRpc2FibGVkIFwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpbmVfdGhyb3VnaCA9IFwiIGxpbmUtdGhyb3VnaCBcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAgJzxkaXYgY2xhc3M9XCJyb3cgYWxlcnQgJyArIGNsYXNzZXMgKyAnIG1iMCBtbDAgbXIwXCI+JyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwiY29sLXNtLTEgcG9zLXJlbC10NCBqcy1zZXQtc3RhdHVzLWRvbmVcIj48aW5wdXQgJyArIGRpc2FibGVkICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3R5cGU9XCJjaGVja2JveFwiICBkYXRhLXN0YXRlPVwiJyArIGRhdGEuc3RhdGUgKyAnXCIgZGF0YS1pZD1cIicgKyBkYXRhLmlkICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ1wiIGNsYXNzPVwiaGVpZ2h0MTIgdzMwXCIvPjwvZGl2PicgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cInNpemUgY29sLXNtLTggJyArIGxpbmVfdGhyb3VnaCArICdcIiA+IDxhIGhyZWY9XCInICsgZGF0YS51cmwgKyAnXCI+JyArIGRhdGEuZGVzY3JpcHRpb24gK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPC9hPjwvZGl2PjxkaXYgY2xhc3M9XCJjb2wtc20tMiBwb3MtcmVsLXQ0XCI+PHNwYW4+JyArIGRhdGEuZGVhZGxpbmUgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPC9zcGFuPjwvZGl2PjxkaXYgY2xhc3M9XCJjb2wtc20tMSBwb3MtcmVsLXQ0IHRleHQtY2VudGVyXCI+PHNwYW4gY2xhc3M9XCJiYWRnZSBiZy1pbmZvXCI+JyArIGRhdGEuY291bnRDb21tZW50cyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8L3NwYW4+PC9kaXY+JyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8L2Rpdj4nO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICBmbkRyYXdDYWxsYmFjazogZnVuY3Rpb24gKGRhdGEpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBfdGhpcy50aW1ldGFibGUgPSBkYXRhLmpzb25bJ3VwZGF0ZXRpbWUnXTtcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMuX2xvYWRlcigpO1xuXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBmblNlcnZlclBhcmFtczogZnVuY3Rpb24gKGFvRGF0YSlcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGFvRGF0YVsnc3RhdGUnXSA9IF90aGlzLnN0YXRlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaWYgKCFwYXJhbXNBcHAuZGV2ZWxvcF9tb2RlKSB7XG4gICAgICAgICAgICAgICAgc2V0SW50ZXJ2YWwoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBfdGhpcy5fcmVsb2FkKCcvdGFzay9pbmRleC90eXBlL2xhc3R1cGRhdGUnLCBfdGhpcy50YXNrdGFibGUpO1xuICAgICAgICAgICAgICAgIH0sIHBhcmFtc0FwcC50aW1lb3V0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sXG4gICAgX2xvYWRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICAkKCcjbG9hZGVyJykuaGlkZSgpO1xuICAgICAgICAkKCcuY29udGVudF90YWJsZScpLnNob3coKTtcbiAgICB9LFxuICAgIF9zZXRGaWx0ZXJEYXRhOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgIHZhciBlbGVtID0gZXZlbnQudGFyZ2V0O1xuICAgICAgICB2YXIgc3R0ID0gJChlbGVtKS5kYXRhKCdmaWx0ZXItc3RhdGUnKTtcbiAgICAgICAgaWYgKHR5cGVvZiAoc3R0KSAhPT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgICAgICAgX3RoaXMuc3RhdGUgPSBzdHQ7XG4gICAgICAgIH1cbiAgICAgICAgX3RoaXMuZmlsdGVyX3RhYmxlID0gJChlbGVtKS5kYXRhKCd0YWJsZS1uYW1lJyk7XG4gICAgICAgIGNvbnNvbGUubG9nKF90aGlzLmZpbHRlcl90YWJsZSk7XG4gICAgICAgIGlmIChfdGhpcy5maWx0ZXJfdGFibGUgPT0gXCJ0YXNrXCIpIHtcbiAgICAgICAgICAgIF90aGlzLnRhc2t0YWJsZS5wYWdlKDApLmRyYXcoZmFsc2UpO1xuICAgICAgICB9XG4gICAgfSxcbiAgICBfcmVsb2FkOiBmdW5jdGlvbiAodXJsLCB0YWJsZSlcbiAgICB7XG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgIHR5cGU6IFwiUE9TVFwiLFxuICAgICAgICAgICAgZGF0YTogXCJsYXN0dXBkYXRlPVwiICsgX3RoaXMudGltZXRhYmxlLFxuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKG1zZylcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZiAobXNnID09IDEpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICB0YWJsZS5kcmF3KGZhbHNlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0sXG59O1xuXG5tb2R1bGUuZXhwb3J0cy5hcHAgPSBBcHA7XG5tb2R1bGUuZXhwb3J0cyA9IEFwcDtcbiIsInZhciBjb21tb25BcHAgPSByZXF1aXJlKCcuL2NvbW1vbicpO1xudmFyIHBhcmFtc0FwcCA9IHJlcXVpcmUoJy4vcGFyYW1zJyk7XG52YXIgZFRhYmxlQXBwID0gcmVxdWlyZSgnLi9kVGFibGUnKTtcblxudmFyIEFwcCA9IHtcbiAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChwYXJhbXNBcHAuZGV2ZWxvcF9tb2RlKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnaW5pdCBkYXRhVGFibGUnKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLl9pbml0SGFuZGxlcigpO1xuICAgIH0sXG4gICAgX2luaXRIYW5kbGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIC8v0L/QvtC80LXRgtC40YLRjCDQutCw0Log0LLRi9C/0L7Qu9C90LXQvdC90YPRjlxuICAgICAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgJy5qcy1zZXQtc3RhdHVzLWRvbmUnLCB0aGlzLl9jaGFuZ2VTdGF0dXNEb25lLmJpbmQodGhpcykpO1xuICAgICAgICAkKCcjYnV0dG9uX2NyZWF0ZU5ld1Rhc2snKS5vbignY2xpY2snLCB0aGlzLl9zYXZlTmV3VGFzay5iaW5kKHRoaXMpKTtcbiAgICB9LFxuICAgIC8v0YHQvtC30LTQsNC90LjQtSDQvdC+0LLQvtC5INC30LDQtNCw0YfQuFxuICAgIF9zYXZlTmV3VGFzazogZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgdHlwZTogXCJwb3N0XCIsXG4gICAgICAgICAgICBkYXRhOiAkKCcjZm9ybUFkZFRhc2snKS5zZXJpYWxpemUoKSxcbiAgICAgICAgICAgIHVybDogXCIvaW5kZXgucGhwL3Rhc2svY3JlYXRlXCIsXG4gICAgICAgICAgICBkYXRhVHlwZTogXCJqc29uXCIsXG4gICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICQoXCIjZm9ybUFkZFRhc2sgW2RhdGEtdmFsXVwiKS5jc3MoXCJib3JkZXJcIiwgXCIxcHggc29saWQgI2NjY1wiKTtcbiAgICAgICAgICAgICAgICAkKFwiI2Zvcm1BZGRUYXNrIFtkYXRhLWVycm9yXVwiKS5oaWRlKCk7XG4gICAgICAgICAgICAgICAgaWYgKGRhdGEuc3RhdHVzID09IFwic3VjY2Vzc1wiKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRUYWJsZUFwcC50YXNrdGFibGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRUYWJsZUFwcC50YXNrdGFibGUuZHJhdyhmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5jbGVhbkZvcm0oXCIjZm9ybUFkZFRhc2tcIik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGRhdGEuc3RhdHVzID09IFwiZXJyb3JcIikge1xuICAgICAgICAgICAgICAgICAgICB2YXIgZGF0YTEgPSBKU09OLnBhcnNlKGRhdGEuZXJyb3JzKTtcbiAgICAgICAgICAgICAgICAgICAgJC5lYWNoKGRhdGExLCBmdW5jdGlvbiAoa2V5LCB2YWwpXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbW1vbkFwcC5jcmVhdGVFcnJvckRpdihrZXksIHZhbCwgJyNmb3JtQWRkVGFzaycpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0sXG4gICAgY2xlYW5Gb3JtOiBmdW5jdGlvbiAoZm9ybSkge1xuICAgICAgICAkKGZvcm0gKyBcIiBbZGF0YS12YWxdXCIpLnZhbCgnJyk7XG4gICAgICAgICQoZm9ybSArIFwiICBbZGF0YS12YWxdXCIpLmNzcyhcImJvcmRlclwiLCBcIjFweCBzb2xpZCAjY2NjXCIpO1xuICAgICAgICAkKGZvcm0gKyBcIiAgW2RhdGEtZXJyb3JdXCIpLmhpZGUoKTtcbiAgICB9LFxuICAgIF9jaGFuZ2VTdGF0dXNEb25lOiBmdW5jdGlvbiAoZSkge1xuICAgICAgICB2YXIgZWxlbSA9ICQoZS50YXJnZXQpO1xuICAgICAgICB2YXIgaWQgPSBlbGVtLmRhdGEoJ2lkJyk7XG5cbiAgICAgICAgaWYgKGlkKSB7XG4gICAgICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgICAgIHR5cGU6IFwicG9zdFwiLFxuICAgICAgICAgICAgICAgIGRhdGE6IFwiaWQ9XCIgKyBpZCxcbiAgICAgICAgICAgICAgICB1cmw6IFwiL2luZGV4LnBocC90YXNrL3NldFN0YXR1c0RvbmVUYXNrXCIsXG4gICAgICAgICAgICAgICAgZGF0YVR5cGU6IFwianNvblwiLFxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLnN0YXR1cyA9PSBcInN1Y2Nlc3NcIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRUYWJsZUFwcC50YXNrdGFibGUpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRUYWJsZUFwcC50YXNrdGFibGUuZHJhdyhmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcuanMtc2V0LXN0YXR1cy1kb25lJykuYXR0cignZGlzYWJsZWQnLCB0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbW1vbkFwcC5zaG93TW9kYWxUYWJzKFwiLmFuY2hvci1tc2dcIiwgXCJzdWNjZXNzXCIsIFwi0JfQsNC00LDRh9CwINC/0L7QvNC10YfQtdC90LAg0LrQsNC6INCy0YvQv9C+0LvQvdC10L3QvdCw0Y9cIiwgMTUwMCk7XG5cblxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29tbW9uQXBwLnNob3dNb2RhbFRhYnMoXCIuYW5jaG9yLW1zZ1wiLCBcImRhbmdlclwiLCBcItCf0YDQvtC40LfQvtGI0LvQsCDQvtGI0LjQsdC60LBcIiwgMTUwMCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cbiAgIFxufTtcbm1vZHVsZS5leHBvcnRzID0gQXBwO1xuXG4iLCJ2YXIgcGFyYW1zID0gcmVxdWlyZSgnLi9wYXJhbXMnKTtcbnZhciBjb21tb24gPSByZXF1aXJlKCcuL2NvbW1vbicpO1xudmFyIGRUYWJsZSA9IHJlcXVpcmUoJy4vZFRhYmxlJyk7XG52YXIgZGF0YVRhYmxlID0gcmVxdWlyZSgnLi9kYXRhVGFibGUnKTtcbnZhciBjb21tZW50ID0gcmVxdWlyZSgnLi9jb21tZW50Jyk7XG4vLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cbnZhciBzdHJNb2R1bGVzID0gJCgnZGl2W2RhdGEtbW9kdWxlc10nKS5kYXRhKCdtb2R1bGVzJyk7XG5pZiAoc3RyTW9kdWxlcykge1xuICAgIHZhciBhcnJNb2R1bGVzID0gc3RyTW9kdWxlcy5zcGxpdChcIiBcIik7XG5cbiAgICBpZiAocGFyYW1zLmRldmVsb3BfbW9kZSkge1xuICAgICAgICBjb25zb2xlLmxvZygnKioqKipzdGFydCBpbml0KioqKionKTtcbiAgICAgICAgY29uc29sZS5sb2coYXJyTW9kdWxlcyk7XG4gICAgfVxuICAgIC8vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG4gICAgaWYgKGFyck1vZHVsZXMuaW5kZXhPZihcInBhcmFtc1wiKSAhPSAtMSkge1xuICAgICAgICBwYXJhbXMuaW5pdCgpO1xuICAgIH1cbiAgICBpZiAoYXJyTW9kdWxlcy5pbmRleE9mKFwiY29tbW9uXCIpICE9IC0xKSB7XG4gICAgICAgIGNvbW1vbi5pbml0KCk7XG4gICAgfVxuICAgIGlmIChhcnJNb2R1bGVzLmluZGV4T2YoXCJkVGFibGVcIikgIT0gLTEpIHtcbiAgICAgICAgZFRhYmxlLmluaXQoKTtcbiAgICB9XG4gICAgaWYgKGFyck1vZHVsZXMuaW5kZXhPZihcImRhdGFUYWJsZVwiKSAhPSAtMSkge1xuICAgICAgICBkYXRhVGFibGUuaW5pdCgpO1xuICAgIH1cbiAgICBpZiAoYXJyTW9kdWxlcy5pbmRleE9mKFwiY29tbWVudFwiKSAhPSAtMSkge1xuICAgICAgICBjb21tZW50LmluaXQoKTtcbiAgICB9XG5cbn0gZWxzZSB7XG4gICAgY29uc29sZS5sb2coXCLQndC1INC40L3QuNGG0LjQsNC70LjQt9C40YDQvtCy0L3RiyDRgdC60YDQuNC/0YLRiyAnL19hc3NldHMvanMvaW5pdC5qcydcIik7XG59XG5cbndpbmRvdy5wYXJhbXMgPSBwYXJhbXM7XG53aW5kb3cuY29tbW9uID0gY29tbW9uO1xud2luZG93LmRUYWJsZSA9IGRUYWJsZTtcbndpbmRvdy5kYXRhVGFibGUgPSBkYXRhVGFibGU7XG53aW5kb3cuY29tbWVudCA9IGNvbW1lbnQ7XG4iLCIgICAgdmFyIEFwcCA9IHtcbiAgICAgICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYoQXBwLmRldmVsb3BfbW9kZSl7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2luaXQgcGFyYW1zJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIFxuICAgICAgICAnc3RhdHVzQWN0aXZlJzoxLFxuICAgICAgICAnc3RhdHVzQmxvY2tlZCcgOjAsICAgICAgXG4gICAgICAgIFxuICAgICAgICAnZGV2ZWxvcF9tb2RlJzogZmFsc2UsIC8v0YDQtdC20LjQvCDRgNCw0LHQvtGC0Ysg0YHQsNC50YLQsCAtIHRydWUgLSDRgNCw0LfRgNCw0LHQvtGC0LrQsCAoINCy0YvQstC+0LTRj9GC0YHRjyDRgdC+0L7QsdGJ0LXQvdC40Y8g0LIg0LrQvtC90YHQvtC70Ywg0Lgg0L7RgtC60LvRjtGH0LXQvdGLINCw0LLRgtC+0L/QvtC00LPRgNGD0LfQutCwINGC0LDQsdC70LjRhiDQuNGC0LQpXG4gICAgICAgICd0aW1lb3V0JzogOTAwMCwgLy/Qv9GA0L7QvNC10LbRg9GC0L7QuiDQvNC10LbQtNGDINC+0LHQvdC+0LLQu9C10L3QuNGP0LzQuCDRgtCw0LHQu9C40YZcbiAgICB9O1xuICAgIG1vZHVsZS5leHBvcnRzID0gQXBwO1xuICAgIFxuICAgICJdfQ==

//# sourceMappingURL=init_glue.js.map
