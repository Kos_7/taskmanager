var gulp = require('gulp');
var watch = require('gulp-watch');
var batch = require('gulp-batch');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var concatCss = require('gulp-concat-css');
var flatten = require('gulp-flatten');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var gutil = require('gulp-util');

var vendorJs = [
  'vendor/app.v2.js',
  'vendor/datatables/jquery.dataTables.js',
  'bower_components/jquery-ui/jquery-ui.min.js',
  'bower_components/rivets/dist/rivets.bundled.min.js',
  'bower_components/rivets/dist/rivets.js',
  'bower_components/lodash/lodash.js',
];

var commonCss = [
  'css/todo/bootstrap.css',
  'css/todo/animate.css',
  'css/todo/app.v2.css',
  'css/todo/font-awesome.min.css',
  'css/todo/font.css',
  'css/todo/plugin.css',
  'css/todo/app.css',
  'css/styles.css',
  'vendor/datatables/datatables.css',
  'css/jquery.dataTables.css',
  'css/jquery-ui.min.css',
  'css/jquery-ui.theme.min.css',
];


// --------------------- JS TASKS ---------------------  //
gulp.task('vendorJs', function () {
  return gulp.src(vendorJs)
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('uglify', function () {
  gulp.src([
    'dist/js/vendor.js',
    'dist/js/common.js'
  ])
    .pipe(uglify('app.min.js', {
      wrap: true
    }))
    .pipe(gulp.dest('dist/js'))
});

gulp.task('browserify', function () {
    var b = browserify({
        entries: ['./js/init.js'],
        debug: true
    });

    return b.bundle()
            .pipe(source('init_glue.js'))
            .pipe(buffer())
            .pipe(sourcemaps.init())
            //.pipe(uglify())
            .on('error', gutil.log)
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest('./dist/js/'));
});

// --------------------- !JS TASKS ---------------------  //

// --------------------- CSS TASKS ---------------------  //

gulp.task('concatCss', function () {
  return gulp.src(commonCss)
    .pipe(concatCss("css/app.css"))
    .pipe(gulp.dest('dist/'));

});

gulp.task('minifyCss', function () {
  return gulp.src('dist/css/app.css')
    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/css'));
});
// --------------------- !CSS TASKS ---------------------  //


gulp.task('watch', function () {
  watch('vendor/**/*.js', batch(function (events, done) {
    gulp.start('vendorJs', done);
  }));
  watch('js/**/*.js', batch(function (events, done) {
    gulp.start('browserify', done);
  }));
  watch('css/**/*.css', batch(function (events, done) {
    gulp.start('concatCss', done);
  }));
});

//prod build
gulp.task('prod', ['vendorJs',  'uglify', 'concatCss', 'minifyCss']);

//local build
gulp.task('default', ['vendorJs', 'concatCss', 'browserify']);




